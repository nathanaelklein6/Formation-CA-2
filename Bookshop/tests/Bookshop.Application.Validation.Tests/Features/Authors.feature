﻿Feature: Add, Delete, Update an author


Scenario: I want to add an auhtor
	Given I have a new author to add
	* I have an Author with firstName <firstName>
	*  a lastName <lastName>
	*  a birthDay date <birthDay>
	* is of gender <gender>
	When I request to add the author
	Then the author is added
	* No exception as been raised
	* the reponse should be an integer
	Examples: 
		| firstName | lastName | birthDay   | gender |
		| Jean      | Doe      | 1985/04/25 | Male   |
		| Jeane     | Dupont   | 1988/01/01 | Female |
