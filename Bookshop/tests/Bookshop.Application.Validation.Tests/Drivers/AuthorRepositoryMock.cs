﻿using Bookshop.Application.Repositories;
using Bookshop.Domain.Entities;

namespace Bookshop.Application.Validation.Tests.Drivers
{
	public class AuthorRepositoryMock : AuthorRepository
	{
		public static void Dispose() 
		{
			Authors.Clear();
		}
		public static List<AuthorDTO> Authors { get; set; } = new List<AuthorDTO>();
		
		public async Task<int> Create(AuthorDTO item)
		{
			await Task.Yield();
			item.CreationDate = DateTime.Now;
			item.ModificationDate= DateTime.Now;
			item.Id = GetId();

			Authors.Add(item);
			return item.Id;
		}

		public Task Delete(int id)
		{
			throw new NotImplementedException();
		}

		public Task<IEnumerable<AuthorDTO>> GetAll()
		{
			throw new NotImplementedException();
		}

		public Task<AuthorDTO> GetById(int id)
		{
			throw new NotImplementedException();
		}

		public Task Update(AuthorDTO item, int id)
		{
			throw new NotImplementedException();
		}

		private int GetId() => Authors.Count + 1;
	}
}
