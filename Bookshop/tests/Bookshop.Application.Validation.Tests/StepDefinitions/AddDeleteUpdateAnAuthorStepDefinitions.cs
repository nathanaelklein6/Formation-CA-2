using Bookshop.Application.Authors.Add;
using Bookshop.Application.Validation.Tests.Drivers;
using Bookshop.Domain.Enums;

namespace Bookshop.Application.Validation.Tests.StepDefinitions
{
	[Binding]
	public class AddDeleteUpdateAnAuthorStepDefinitions :Testing, IDisposable
	{
		private AddAuthorCommand? _addAuthorCommand;
		private object? _response;
		private Exception? _exception;

		[Given(@"I have a new author to add")]
		public void GivenIHaveANewAuthorToAdd()
		{
			_addAuthorCommand = new AddAuthorCommand();
		}

		[Given(@"I have an Author with firstName (.*)")]
		public void GivenIHaveAnAuthorWithFirstName(string firstName)
		{
			_addAuthorCommand.FirstName = firstName;
		}

		[Given(@"a lastName (.*)")]
		public void GivenALastName(string lastName)
		{
			_addAuthorCommand.LastName = lastName;
		}

		[Given(@"a birthDay date (.*)")]
		public void GivenABirthDayDate(DateTime birthDay)
		{
			_addAuthorCommand.BirthDay = birthDay;
		}

		[Given(@"is of gender (.*)")]
		public void GivenIsOfGender(Gender gender)
		{
			_addAuthorCommand.Gender = gender;
		}

		[When(@"I request to add the author")]
		public async Task WhenIRequestToAddTheAuthor()
		{
			try
			{
				_response = await SendAsync(_addAuthorCommand);
			}
			catch (Exception e)
			{
				_exception = e;
			}
		}

		[Then(@"No exception as been raised")]
		public void ThenNoExceptionAsBeenRaised()
		{
			_exception.Should().BeNull();
		}

		[Then(@"the reponse should be an integer")]
		public void ThenTheReposeShouldBeAnInteger()
		{
			_response.Should().NotBeNull();
			_response.Should().BeOfType<int>();
		}

		[Then(@"the author is added")]
		public void ThenTheAuthorIsAdded()
		{
			AuthorRepositoryMock.Authors.Should().HaveCount(1);
		}

		public void Dispose() => 
			AuthorRepositoryMock.Dispose();
			
	}
}
