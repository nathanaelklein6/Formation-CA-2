﻿using Bookshop.Application.Common.Behaviors;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Formation.Application;

public static class DependencyInjection
{
	public static IServiceCollection AddApplication(this IServiceCollection services)
	{
		var ass = Assembly.GetExecutingAssembly();
		services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
		services.AddMediatR(Assembly.GetExecutingAssembly());

		services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehavior<,>));
		//Au choix on peut utiliser l'extention mediatr ou celle maison l'avantage de celle maison est la customisation
		//services.AddTransient(typeof(IPipelineBehavior<,>), typeof(Bookshop.Application.Common.Behaviors.ValidationBehavior<,>)); 
		services.AddTransient(typeof(IPipelineBehavior<,>), typeof(MediatR.Extensions.FluentValidation.AspNetCore.ValidationBehavior<,>));

		return services;
	}
}
