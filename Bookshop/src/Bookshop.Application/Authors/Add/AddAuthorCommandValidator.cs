﻿using FluentValidation;

namespace Bookshop.Application.Authors.Add
{
	public class AddAuthorCommandValidator : AbstractValidator<AddAuthorCommand>
	{
		public AddAuthorCommandValidator()
		{
			RuleFor(a => a.FirstName)
				.NotNull().WithMessage("{PropertyName} Should not be null")
				.NotEmpty().WithMessage("{PropertyName} Should not be empty");
			RuleFor(a => a.LastName)
				.NotNull().WithMessage("{PropertyName} Should not be null")
				.NotEmpty().WithMessage("{PropertyName} Should not be empty");
			RuleFor(a => a.BirthDay)
				.NotNull().WithMessage("{PropertyName} Should not be null")
				.LessThan(DateTime.Now.AddYears(-3)).WithMessage("The author must be older than 3 years");
		}
	}
}
