﻿using Bookshop.Application.Repositories;
using MediatR;

namespace Bookshop.Application.Authors.Add
{
	public class AddAuthorCommandHandler : IRequestHandler<AddAuthorCommand, int>
	{
		private readonly AuthorRepository _authorRepository;

		public AddAuthorCommandHandler(AuthorRepository authorRepository)
		{
			_authorRepository = authorRepository;
		}

		public async Task<int> Handle(AddAuthorCommand request, CancellationToken cancellationToken)
		{
			return await _authorRepository
				.Create(new Domain.Entities.AuthorDTO
				{
					FirstName = request.FirstName,
					LastName = request.LastName,
					BirthDay = request.BirthDay,
					Gender = request.Gender
				});
		}
	}
}
