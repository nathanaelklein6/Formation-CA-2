﻿using Bookshop.Domain.Enums;
using MediatR;

namespace Bookshop.Application.Authors.Add
{
	public class AddAuthorCommand : IRequest<int>
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime BirthDay { get; set; }
		public Gender Gender { get; set; }
	}
}
