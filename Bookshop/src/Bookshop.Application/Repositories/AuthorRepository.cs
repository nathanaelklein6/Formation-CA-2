﻿using Bookshop.Domain.Entities;

namespace Bookshop.Application.Repositories
{
	public interface AuthorRepository : GenericRepository<AuthorDTO>
	{
	}
}
