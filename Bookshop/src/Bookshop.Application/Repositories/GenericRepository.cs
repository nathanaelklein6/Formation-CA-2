﻿using Bookshop.Domain.Entities;

namespace Bookshop.Application.Repositories
{
	public interface GenericRepository<T> where T : BaseEntity
	{
		Task<IEnumerable<T>> GetAll();
		Task<int> Create(T item);
		Task<T> GetById(int id);
		Task Update(T item, int id);
		Task Delete(int id);
	}

}
