﻿using AutoMapper;
using Bookshop.Application.Repositories;
using Bookshop.Domain.Entities;
using Bookshop.Infrastructure.Entities;
using Bookshop.Infrastructure.Persistence;

namespace Bookshop.Infrastructure.Repositories
{
	public class AuthorRepositoryImp : AuthorRepository
	{
		private BookshopDbContext _context;
		private IMapper _mapper;

		public AuthorRepositoryImp(BookshopDbContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		public async Task<int> Create(AuthorDTO item)
		{
			var author = _mapper.Map<Author>(item);

			_context.Authors.Add(author);

			await _context.SaveChangesAsync();

			return author.Id;
		}

		public Task Delete(int id)
		{
			throw new NotImplementedException();
		}

		public Task<IEnumerable<AuthorDTO>> GetAll()
		{
			throw new NotImplementedException();
		}

		public Task<AuthorDTO> GetById(int id)
		{
			throw new NotImplementedException();
		}

		public Task Update(AuthorDTO item, int id)
		{
			throw new NotImplementedException();
		}
	}
}
