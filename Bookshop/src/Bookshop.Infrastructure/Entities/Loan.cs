﻿using Bookshop.Domain.Enums;

namespace Bookshop.Infrastructure.Entities
{
	public class Loan : BaseEntity
	{
		public Book Book { get; set; }
		public Customer Customer { get; set; }
		public DateTime ReturnDate { get; set; }
		public LoanStatus Status { get; set; }
	}
}
