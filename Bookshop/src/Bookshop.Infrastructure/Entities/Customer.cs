﻿using Bookshop.Domain.Enums;

namespace Bookshop.Infrastructure.Entities
{
	public class Customer : BaseEntity
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime BirthDay { get; set; }
		public Gender Gender { get; set; }
		public ICollection<Loan> Loans { get; set; }
	}
}
