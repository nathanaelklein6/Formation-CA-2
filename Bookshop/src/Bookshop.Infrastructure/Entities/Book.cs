﻿namespace Bookshop.Infrastructure.Entities
{
	public class Book : BaseEntity
	{
		public string Title { get; set; }
		public string Description { get; set; }
		public Author Author { get; set; }
		public DateOnly PublicationDate { get; set; }
		public int NumberOfPage { get; set; }
	}
}