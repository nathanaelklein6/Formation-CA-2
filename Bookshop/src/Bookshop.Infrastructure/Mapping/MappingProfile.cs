﻿using AutoMapper;
using Bookshop.Infrastructure.Entities;

namespace Bookshop.Infrastructure.Mapping
{
	internal class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<Domain.Entities.BaseEntity, BaseEntity>()
				.ReverseMap();
			CreateMap<Domain.Entities.AuthorDTO, Author>()
				.ReverseMap();
			CreateMap<Domain.Entities.BookDTO, Book>()
				.ReverseMap();
			CreateMap<Domain.Entities.CustomerDTO, Customer>()
				.ReverseMap();
			CreateMap<Domain.Entities.LoanDTO, Loan>()
				.ReverseMap();
		}
	}
}
