﻿using Bookshop.Application.Repositories;
using Bookshop.Infrastructure.Persistence;
using Bookshop.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Bookshop.Infrastructure
{
	public static class DependencyInjection
	{
		public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
		{
			AddContext(services, configuration);

			services.AddAutoMapper(Assembly.GetExecutingAssembly());
			AddRepositories(services);

			return services;
		}

		private static void AddRepositories(IServiceCollection services)
		{
			services.AddScoped<AuthorRepository, AuthorRepositoryImp>();
		}

		private static void AddContext(IServiceCollection services, IConfiguration configuration)
		{
			var p = configuration.GetSection("Sqlite").Value;

			services.AddDbContext<BookshopDbContext>(
				options => options
					.UseSqlite(configuration.GetSection("Sqlite").Value));
		}
	}
}
