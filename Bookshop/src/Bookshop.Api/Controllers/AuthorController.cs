﻿using Bookshop.Application.Authors.Add;
using Bookshop.Application.Common.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Bookshop.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AuthorController : ControllerBase
	{
		private readonly ISender _mediator;

		public AuthorController(ISender mediator)
		{
			_mediator = mediator;
		}

		[HttpPost]
		public async Task<IActionResult> Add([FromBody] AddAuthorCommand command)
			=> await Send(command);

		private async Task<IActionResult> Send<T>(IRequest<T> request)
		{
			try
			{
				return Ok(await _mediator.Send(request));
			}
			catch (ValidationException ex)
			{
				return BadRequest(ex.Errors.Select(x => x.Value));
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception e)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
			}
		}
	}
}
