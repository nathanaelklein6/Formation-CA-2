﻿using Bookshop.Domain.Enums;

namespace Bookshop.Domain.Entities
{
	public class LoanDTO : BaseEntity
	{
		public BookDTO Book { get; set; }
		public CustomerDTO Customer { get; set; }
		public DateTime ReturnDate { get; set; }
		public LoanStatus Status { get; set; }
	}
}
