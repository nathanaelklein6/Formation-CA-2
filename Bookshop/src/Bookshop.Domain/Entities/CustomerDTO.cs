﻿using Bookshop.Domain.Enums;

namespace Bookshop.Domain.Entities
{
	public class CustomerDTO : BaseEntity
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime BirthDay { get; set; }
		public Gender Gender { get; set; }
		public ICollection<LoanDTO> Loans { get; set; }
	}
}
