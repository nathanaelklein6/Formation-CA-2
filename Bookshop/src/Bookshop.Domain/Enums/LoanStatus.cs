﻿namespace Bookshop.Domain.Enums
{
	public enum LoanStatus
	{
		InProgress,
		Returned
	}
}
